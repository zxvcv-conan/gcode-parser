# Copyright (c) 2020-2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file

cmake_minimum_required(VERSION 3.15)
project(gcode-parser C)


# create library
add_library(${CMAKE_PROJECT_NAME}
    src/action.c
)

target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC
    include
)

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES
    PUBLIC_HEADER "include/gcode_parser.h"
)

install(TARGETS ${CMAKE_PROJECT_NAME})

# add_subdirectory(unittest)
