// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include "_commands.h"


// ============================== PRIVATE DEFINITIONS =================================

Std_Err init_G91(GCode_Settings* settings, GCodeCommand* cmd)
{
    cmd->remove = NULL;
    cmd->step = NULL;

    settings->positioning_mode = RELATIVE;

    return STD_OK;
}
