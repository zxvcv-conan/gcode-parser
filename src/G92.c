// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include "_commands.h"


// ============================== PRIVATE DEFINITIONS =================================

Std_Err init_G92(GCode_Settings* settings, GCodeCommand* cmd)
{
    cmd->remove = NULL;
    cmd->step = NULL;

    Motor** motors = settings->motors;

    Std_Err stdErr;

    for(uint8_t i=PARAM_X; i<=PARAM_E; i<<=1)
    {
        if(cmd->used_fields & i)
        {
            stdErr = motor_set_position(motors[MOTOR_X], cmd->data.x);
            if(stdErr != STD_OK)
            {
                return stdErr;
            }
        }
    }

    return STD_OK;
}
