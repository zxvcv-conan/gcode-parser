// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include "_commands.h"


// ============================== PRIVATE DEFINITIONS =================================

extern Std_Err step_G2(GCode_Settings* settings, GCodeCommand* cmd);
extern Std_Err remove_G2(GCode_Settings* settings, GCodeCommand* cmd);
extern Std_Err init_circle_movement(GCode_Settings* settings, GCodeCommand* cmd);

Std_Err init_G3(GCode_Settings* settings, GCodeCommand* cmd)
{
    Std_Err stdErr = STD_OK;

    cmd->remove = remove_G2;
    cmd->step = step_G2;

    settings->circle_move_mode = COUNTER_CLOCKWISE_CIRCLE;

    stdErr = init_circle_movement(settings, cmd);

    return stdErr;
}
