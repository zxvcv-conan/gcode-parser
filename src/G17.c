// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include "_commands.h"


// ============================== PRIVATE DEFINITIONS =================================

Std_Err init_G17(GCode_Settings* settings, GCodeCommand* cmd)
{
    cmd->remove = NULL;
    cmd->step = NULL;

    settings->plane_selection.plane_x = 1;
    settings->plane_selection.plane_y = 1;
    settings->plane_selection.plane_z = 0;

    return STD_OK;
}
