Changelog
=========

0.1.1 (2022-11-07)
------------------
- Doc comments structure updates.

0.1.0 (2022-11-07)
------------------
- Initial.